'use strict';

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import {
    AppRegistry,
} from 'react-native';

import ReactNativeGreGApp from "./app/index.js";

AppRegistry.registerComponent('ReactNativeGreGApp', () => ReactNativeGreGApp);
