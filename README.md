# ReactNativeGreGApp

Première application que je réalise en ReactNative, on va bien voir ce que ça donne !

## Run Android App

```
react-native start
react-native run-android
```

## Troubleshooting

Dans le cas de cette erreur lors du react-native start
```
ERROR  A non-recoverable condition has triggered.  Watchman needs your help!
The triggering condition was at timestamp=1462711910: inotify-add-watch(/home/greg/Documents/AwesomeProject/node_modules/react-native/node_modules/fbjs-scripts/node_modules/babel/node_modules/babel-core/node_modules/resolve/test/resolver/punycode/node_modules) -> The user limit on the total number of inotify watches was reached; increase the fs.inotify.max_user_watches sysctl
All requests will continue to fail with this message until you resolve
the underlying problem.  You will find more information on fixing this at
https://facebook.github.io/watchman/docs/troubleshooting.html#poison-inotify-add-watch
```

La solution est la suivante (voir ce [lien](https://github.com/facebook/react-native/issues/3199 "Error when running watchman #3199"))
```
echo 256 | sudo tee -a /proc/sys/fs/inotify/max_user_instances
echo 32768 | sudo tee -a /proc/sys/fs/inotify/max_queued_events
echo 65536 | sudo tee -a /proc/sys/fs/inotify/max_user_watches
watchman shutdown-server
```

Pour lancer l'émulateur Android directement
```
/home/greg/Android/Sdk/tools/emulator -netdelay none -netspeed full -avd react-native
```
