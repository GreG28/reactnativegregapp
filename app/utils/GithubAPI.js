'use strict';

export let GithubAPI = {
    REQUEST_URL : 'https://api.github.com',
    methods : {
        code_search_url : 'code_search_url'
    },
    getUrlMethod : function(method) {
        return this.REQUEST_URL + '/' + method;
    }
};
