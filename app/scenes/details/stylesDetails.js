'use strict';

import { StyleSheet } from 'react-native';

const stylesDetails = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    }
});

export default stylesDetails;
