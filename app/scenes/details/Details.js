'use strict';

import React, {
    Component
} from 'react';
import {
    Text,
    View
} from 'react-native';

import stylesDetails from './stylesDetails.js';

const REQUEST_URL = 'https://raw.githubusercontent.com/facebook/react-native/master/docs/MoviesExample.json';

export default class Details extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Text>Details</Text>
            </View>
        );
    }
};
