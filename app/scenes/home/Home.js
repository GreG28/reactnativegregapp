'use strict';

import React, {
    Component
} from 'react';
import {
    Image,
    Text,
    ListView,
    View,
    TouchableHighlight
} from 'react-native';

import stylesHome from './stylesHome.js';

const REQUEST_URL = 'https://raw.githubusercontent.com/facebook/react-native/master/docs/MoviesExample.json';

export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataSource: new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2,
            }),
            loaded: false,
        };
    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        fetch(REQUEST_URL)
            .then((response) => response.json())
            .then((responseData) => {
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(responseData.movies),
                    loaded: true,
                });
            })
            .done();
    }

    render() {
        if (!this.state.loaded) {
            return this.renderLoadingView();
        }

        return (
            <ListView dataSource={this.state.dataSource}
                renderRow={(movie) => this.renderMovie(movie, this)}
                style={stylesHome.listView}
                />
        );
    }

    renderLoadingView() {
        return (
            <View style={stylesHome.container}>
                <Text>Loading movies...</Text>
            </View>
        );
    }

    _onKeyPress(movie) {
        console.log("on Key press Movie -> " + movie.title);
        this.props.onPushRoute();
    }

    renderMovie(movie, parent) {
        return (
            <View>
                <TouchableHighlight
                    onPress={parent._onKeyPress.bind(parent, movie)}
                    >
                    <View style={stylesHome.container} >
                        <Image source={{uri: movie.posters.thumbnail}}
                            style={stylesHome.thumbnail} />
                        <View style={stylesHome.rightContainer} >
                            <Text style={stylesHome.title} >
                                {movie.title}
                            </Text>
                            <Text style={stylesHome.year} >{movie.year}</Text>
                        </View>
                    </View>
                </TouchableHighlight>
            </View>
        );
    }
};
