'use strict';

import { StyleSheet } from 'react-native';

const stylesHome = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    },
    rightContainer: {
        flex: 1
    },
    thumbnail: {
        width: 50,
        height: 81
    },
    title: {
        fontSize: 25,
        marginBottom: 8,
        textAlign: 'center'
    },
    year: {
        textAlign: 'center'
    },
    listView: {
        flex: 1,
        paddingTop: 20,
        backgroundColor: '#F5FCFF',
    }
});

export default stylesHome;
